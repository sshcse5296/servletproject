package com.servlet.user;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class UserHolder {

	private static Map<String, User> USER_HOLDER = null;

	static {
		USER_HOLDER = new HashMap<>();
	}

	public static  User getUser(String username) {
		return USER_HOLDER.get(username);
	}

	public static void setUser(User user) {
		USER_HOLDER.put(user.getUsername(), user);
	}
	
	public static Collection<User> getAllUser() {
		return USER_HOLDER.values();
	}
	
	

}
