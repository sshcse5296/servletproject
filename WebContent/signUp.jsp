<%@ include file="jstlTage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="css/comman.css" />
</head>
<body>
<%@ include file="header.jsp"%>
<div class="container">
<div class="wid-33"></div>
<div class="wid-33" style="background: #cecdbf;">
 <form action="user" method="post">
    <label for="username"> UserName</label>
    <input type="text" name="username" value="${param.username}"><br>
    <span class="custom-error">${username_error}</span>
    <label for="password"> Password</label>
    <input type="text" name="password" value="${param.password}"><br>
    <span class="custom-error">${pasword_error}</span>
    <label for="firstname">First Name</label>
    <input type="text" name="firstname" value="${param.firstname}"><br>
    <span class="custom-error">${firstname_error}</span>
    <label for="lastName">Last Name</label>
    <input type="text" name="lastname" value="${param.lastname}"><br>
    <span class="custom-error">${lastname_error}</span>
    <label for="address">Address  </label>
    <input type="text" name="address" value="${param.address}"><br>
    <span class="custom-error">${address_error}</span>
    <label for="contactNo">Contact</label>
    <input type="text" name="contactNo" value="${param.contactNo}"><br>
    <span class="custom-error">${contactNo_error}</span>
    <label for="gender">Gender</label>
    <c:choose>
  		<c:when test="${param.gender=='male'}">
     		 <input type="radio"  checked name="gender" value="male"> Male
 		 </c:when>
  		<c:otherwise>
      		 <input type="radio" name="gender" value="male"> Male
  		</c:otherwise>
	</c:choose>
	
	 <c:choose>
  		<c:when test="${param.gender=='female'}">
     		 <input type="radio"  checked name="gender" value="female"> Female
 		 </c:when>
  		<c:otherwise>
     		 <input type="radio" name="gender" value="female"> Female 
  		</c:otherwise>
	</c:choose>
	 <c:choose>
  		<c:when test="${param.gender=='other'}">
  			 <input type="radio" checked name="gender" value="other"> Other<br>
 		 </c:when>
  		<c:otherwise>
  			 <input type="radio" name="gender" value="other"> Other<br>
  		</c:otherwise>
	</c:choose>
    <br>
    <span class="custom-error">${gender_error}</span>
    <label for=courseList>Course</label>
   
    <c:set var="cString" value=""/>
    <c:forEach items="${paramValues['courseList']}" var="course">
        <c:set var="cString" value="${cString.concat(course)}"/>
    </c:forEach>
    <input  <c:if test="${cString.contains('C')}">checked="checked"</c:if> type="checkbox" name="courseList" value="C">C
    <input  <c:if test="${cString.contains('JAVA')}">checked="checked"</c:if>  type="checkbox" name="courseList" value="JAVA">JAVA
    <input  <c:if test="${cString.contains('KOTLIN')}">checked="checked"</c:if>  type="checkbox" name="courseList" value="KOTLIN">KOTLIN
    <input  <c:if test="${cString.contains('PYTHON')}">checked="checked"</c:if>  type="checkbox" name="courseList" value="PYTHON">PYTHON
    <span class="custom-error">${courseList_error}</span>
    <input type="submit" value="Submit">
 </form>
</div>
<div class="wid-33"></div>
</div>
<%@ include file="footer.jsp"%>
</body>
</html>