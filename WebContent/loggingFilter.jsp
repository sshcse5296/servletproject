<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.servlet.user.LoginServlet"%>
<%@ include file="jstlTage.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="/CourseManagement/css/comman.css" />
</head>
<body>
	<div class="container">
		<div class="wid-33"></div>
		<div class="wid-33" style="background: #cecdbf; margin-top: 100px;">
			<h1>Enter User and Password</h1>
			<form action="LoggingFilter">
				<label for="username"> UserName</label>
				<input type="text" name="username"><br>
				<span class="custom-error"> 
				</span> <label for="password"> Password</label>
				<input type="text" name="password" ><br>
				<span class="custom-error"> </span>
				<input type="submit" value="Sign IN">
				<a class=" ha active" href="/CourseManagement/signUp.jsp">Sign UP</a>
			</form>
			<div class="wid-33"></div>
		</div>
	</div>
	<%@ include file="footer.jsp"%>
</body>
</html>