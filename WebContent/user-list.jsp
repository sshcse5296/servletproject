<%@ include file="jstlTage.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="/CourseManagement/css/comman.css" />
</head>
<body>
<%@ include file="header.jsp"%>
	<div class="container">
	<div>
		<table class="table">
		<thead>
			<tr>
				<th>Username</th>
				<th>Password</th>
				<th>Firstname</th>
				<th>Lastname</th>
				<th>ContectNo</th>
				<th>Address</th>
				<th>Gender</th>
				<th>CourseList</th>
			</tr>
			</thead>
			<tbody>
		   <c:forEach items="${userList}" var="user">
			<tr>
				<td>${user.getUsername()}</td>
				<td>${user.getPassword()}</td>
				<td>${user.getFirstname()}</td>
				<td>${user.getLastname()}</td>
				<td>${user.getContactNo()}</td>
				<td>${user.getAddress()}</td>
				<td>${user.getGender()}</td>
				<td>${user.getCourseList()}</td>
			</tr>
			</c:forEach>
			</tbody>
		</table>
	</div>
	</div>
<%@ include file="footer.jsp"%>
</body>
</html>