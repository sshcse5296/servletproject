<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.servlet.user.User"%>
<%@ include file="jstlTage.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="css/comman.css" />
</head>
<body>
<%@ include file="header.jsp"%>
<div class="container">
<div class="wid-33"></div>
<div class="wid-33" style="font-size: 34px;font-weight: 500;">  
    
	Welcome  <%
	User user=(User)request.getSession().getAttribute("user"); 
	if (user== null) { %>
		<a href='/CourseManagement/login.jsp'>Login</a>
	<%}else{%>
		<%=user.getUsername() %>
	<%}%>
	
	<table border="2" cellpadding="2" cellspacing="2">
  <thead> 
  <tr valign="TOP"> 
    <td colspan="1" rowspan="1" valign="TOP" width="99"> 
      <p><font size="-1"><b>Implicit Object</b></font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="69"> 
      <p><font size="-1"><b>Type</b></font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="101"> 
      <p><font size="-1"><b>Key<a href="javascript:popUp('/content/images/chap2_0131001531/elementLinks/ch02fn12.html')"><sup></sup></a></b></font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="94"> 
      <p><font size="-1"><b>Value</b></font></p>
    </td>
  </tr>
  </thead><tbody> 
  <tr valign="TOP"> 
    <td colspan="1" rowspan="1" valign="TOP" width="99"> 
      <p><tt><font size="-1">cookie</font></tt></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="69"> 
      <p><font size="-1">Map</font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="101"> 
      <p><font size="-1">Cookie name</font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="94"> 
      <p><font size="-1">Cookie</font></p>
    </td>
  </tr>
  <tr valign="TOP"> 
    <td colspan="1" rowspan="1" valign="TOP" width="99"> 
      <p><tt><font size="-1">header</font></tt></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="69"> 
      <p><font size="-1">Map</font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="101"> 
      <p><font size="-1">Request header name</font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="94"> 
      <p><font size="-1">Request header value</font></p>
    </td>
  </tr>
  <tr valign="TOP"> 
    <td colspan="1" rowspan="1" valign="TOP" width="99"> 
      <p><tt><font size="-1">headerValues</font></tt></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="69"> 
      <p><font size="-1">Map</font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="101"> 
      <p><font size="-1">Request header name</font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="94"> 
      <p><font size="-1">String[] of request header values</font></p>
    </td>
  </tr>
  <tr valign="TOP"> 
    <td colspan="1" rowspan="1" valign="TOP" width="99"> 
      <p><tt><font size="-1">initParam</font></tt></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="69"> 
      <p><font size="-1">Map</font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="101"> 
      <p><font size="-1">Initialization parameter name</font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="94"> 
      <p><font size="-1">Initialization parameter value</font></p>
    </td>
  </tr>
  <tr valign="TOP"> 
    <td colspan="1" rowspan="1" valign="TOP" width="99"> 
      <p><tt><font size="-1">param</font></tt></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="69"> 
      <p><font size="-1">Map</font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="101"> 
      <p><font size="-1">Request parameter name</font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="94"> 
      <p><font size="-1">Request parameter value</font></p>
    </td>
  </tr>
  <tr valign="TOP"> 
    <td colspan="1" rowspan="1" valign="TOP" width="99"> 
      <p><tt><font size="-1">paramValues</font></tt></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="69"> 
      <p><font size="-1">Map</font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="101"> 
      <p><font size="-1">Request parameter name</font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="94"> 
      <p><font size="-1">String[] of request parameter values</font></p>
    </td>
  </tr>
  <tr valign="TOP"> 
    <td colspan="1" rowspan="1" valign="TOP" width="99"> 
      <p><tt><font size="-1">pageContext</font></tt></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="69"> 
      <p><font size="-1">PageContext</font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="101"> 
      <p><font size="-1">N/A</font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="94"> 
      <p><font size="-1">N/A</font></p>
    </td>
  </tr>
  <tr valign="TOP"> 
    <td colspan="1" rowspan="1" valign="TOP" width="99"> 
      <p><tt><font size="-1">pageScope</font></tt></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="69"> 
      <p><font size="-1">Map</font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="101"> 
      <p><font size="-1">Page-scoped attribute name</font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="94"> 
      <p><font size="-1">Page-scoped attribute value</font></p>
    </td>
  </tr>
  <tr valign="TOP"> 
    <td colspan="1" rowspan="1" valign="TOP" width="99"> 
      <p><tt><font size="-1">requestScope</font></tt></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="69"> 
      <p><font size="-1">Map</font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="101"> 
      <p><font size="-1">Request-scoped attribute name</font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="94"> 
      <p><font size="-1">Request-scoped attribute value</font></p>
    </td>
  </tr>
  <tr valign="TOP"> 
    <td colspan="1" rowspan="1" valign="TOP" width="99"> 
      <p><tt><font size="-1">sessionScope</font></tt></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="69"> 
      <p><font size="-1"><tt>Map</tt></font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="101"> 
      <p><font size="-1">Session-scoped attribute name</font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="94"> 
      <p><font size="-1">Session-scoped attribute value</font></p>
    </td>
  </tr>
  <tr valign="TOP"> 
    <td colspan="1" rowspan="1" valign="TOP" width="99"> 
      <p><tt><font size="-1">applicationScope</font></tt></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="69"> 
      <p><font size="-1"><tt>Map</tt></font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="101"> 
      <p><font size="-1">Application-scoped attribute name</font></p>
    </td>
    <td colspan="1" rowspan="1" valign="TOP" width="94"> 
      <p><font size="-1">Application-scoped attribute value</font></p>
    </td>
  </tr>
  </tbody> 
</table>
</div>
</div>
<%@ include file="footer.jsp"%>
</body>
</html>