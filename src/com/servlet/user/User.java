package com.servlet.user;

import java.util.Set;

public class User {

	String username;
	String password;
	String firstname;
	String lastname;
	String address;
	String contactNo;
	String gender;
	Set<String> courseList;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Set<String> getCourseList() {
		return courseList;
	}

	public void setCourseList(Set<String> courseList) {
		this.courseList = courseList;
	}

	@Override
	public String toString() {
		return "User [username=" + username + ", password=" + password + ", firstname=" + firstname + ", lastname="
				+ lastname + ", address=" + address + ", contactNo=" + contactNo + ", gender=" + gender
				+ ", courseList=" + courseList + "]";
	}
	
	

}
