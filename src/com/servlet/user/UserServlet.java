package com.servlet.user;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet("/user")
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	
	public void init(ServletConfig	 servletConfig) throws ServletException {
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		boolean status=false;
		if (username==null || username.isEmpty()) {
			request.setAttribute("username_error", "Please enter username");
			   status=true;
		} 
		String password = request.getParameter("password");
		if (password==null || password.isEmpty()) {
			request.setAttribute("pasword_error", "Please enter password");
			status=true;
		}
		String firstname = request.getParameter("firstname");
		if (firstname==null || firstname.isEmpty()) {
			request.setAttribute("firstname_error", "Please enter firstname");
			status=true;
		}
		String lastname = request.getParameter("lastname");
		if (lastname==null || lastname.isEmpty()) {
			request.setAttribute("lastname_error", "Please enter lastname");
			status=true;
		}
		String address = request.getParameter("address");
		if (address==null || address.isEmpty()) {
			request.setAttribute("address_error", "Please enter address");
			status=true;
		}
		String contactNo = request.getParameter("contactNo");
		if (contactNo==null || contactNo.isEmpty()) {
			request.setAttribute("contactNo_error", "Please enter contactNo");
			status=true;
		}
		String gender = request.getParameter("gender");
		if (gender==null || gender.isEmpty()) {
			request.setAttribute("gender_error", "Please enter gender");
			status=true;
		}
		String[] courses = request.getParameterValues("courseList");
		if (courses==null || courses.length==0) {
			request.setAttribute("courseList_error", "Please enter courseList");
			status=true;
		}
		if(status) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("signUp.jsp");
			dispatcher.include(request, response);
			return;
		}
		Set<String> courseList = new HashSet<>(Arrays.asList(courses));
		
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setFirstname(firstname);
		user.setLastname(lastname);
		user.setAddress(address);
		user.setContactNo(contactNo);
		user.setGender(gender);
		user.setCourseList(courseList);
		UserHolder.setUser(user);
		response.sendRedirect("login.jsp");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		
		/*
		 * Enumeration<String> getParameterNamesEnum = request.getParameterNames();
		 * while (getParameterNamesEnum.hasMoreElements()) { String param = (String)
		 * getParameterNamesEnum.nextElement(); String value =
		 * request.getParameter(param); System.out.println("GetParameterName[" + param +
		 * "]- " + value); }
		 * 
		 * Enumeration<String> getAttributeNamesEnum = request.getAttributeNames();
		 * while (getAttributeNamesEnum.hasMoreElements()) { String param = (String)
		 * getAttributeNamesEnum.nextElement(); Object value =
		 * request.getAttribute(param); System.out.println("GetAttributeName[ " + param
		 * + " ]- " + value); }
		 * 
		 * Enumeration<String> getHeaderNamesEnum = request.getHeaderNames(); while
		 * (getHeaderNamesEnum.hasMoreElements()) { String param = (String)
		 * getHeaderNamesEnum.nextElement(); String value = request.getHeader(param);
		 * System.out.println("GetHeaderName[ " + param + " ]- " + value); }
		 * 
		 * //1-request.getParameterMap(); Map<String, String[]> parameterMap =
		 * request.getParameterMap(); for(String param:parameterMap.keySet()) {
		 * System.out.println("GetParameterMap[ " + param + " ]- " +
		 * parameterMap.get(param)); } //2-request.getPathInfo();
		 * System.out.println("Get PathInfo-"+request.getPathInfo());
		 * 
		 * //3-request.getRequestURI();
		 * System.out.println("Get RequestURI-"+request.getRequestURI());
		 * 
		 * //4-request.getRequestURL();
		 * System.out.println("Get RequestURL-"+request.getRequestURL());
		 * 
		 * //5-request.getServerName();
		 * System.out.println("Get getServerName-"+request.getServerName());
		 * 
		 * //6-request.getQueryString();
		 * System.out.println("Get QueryString-"+request.getQueryString());
		 * 
		 * //7-request.getContextPath();
		 * System.out.println("Get ContextPath-"+request.getContextPath());
		 * 
		 * //8.request.getServletPath();
		 * System.out.println("Get ServletPath-"+request.getServletPath());
		 * 
		 * //9-request.getHttpServletMapping();
		 * System.out.println("Get ServletPath-"+request.getHttpServletMapping());
		 */
	
}
