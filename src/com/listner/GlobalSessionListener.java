package com.listner;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Application Lifecycle Listener implementation class SessionListener
 *
 */
@WebListener
public class GlobalSessionListener implements HttpSessionListener {

	public void sessionCreated(HttpSessionEvent e) {
		System.out.println("sessionCreated.......");
	}

	public void sessionDestroyed(HttpSessionEvent e) {
		System.out.println("sessionDestroyed..........");
	}
}
