<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.servlet.user.LoginServlet"%>
<%@ include file="jstlTage.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="/CourseManagement/css/comman.css" />
</head>
<body>
	<div class="container">
		<div class="wid-33"></div>
		<div class="wid-33" style="background: #cecdbf; margin-top: 100px;">
			<h1>Enter User and Password</h1>
			<form action="login" method="post">
				<label for="username"> UserName</label>
				<%
					String username = request.getParameter("username");
					if (username == null) {
						username = "";
					}
				%>
				<input type="text" name="username" value="<%=username%>"><br>
				<span class="custom-error"> 
				<%
 	            if (request.getAttribute("username_error") != null) {
 		               out.println(request.getAttribute("username_error"));
 	                     }
                 %>
				</span> <label for="password"> Password</label>
				<%
					String password = request.getParameter("password");
					if (password == null) {
						password = "";
					}
				%>
				<input type="text" name="password" value="<%=password%>"><br>
				<span class="custom-error"> 
				<%
             	if (request.getAttribute("password_error") != null) {
 		          out.println(request.getAttribute("password_error"));
 	            }
                %>
				</span>
				<input type="submit" value="Sign IN">
				<a class=" ha active" href="/CourseManagement/signUp.jsp">Sign UP</a>
			</form>
			<div class="wid-33"></div>
		</div>
	</div>
	<%@ include file="footer.jsp"%>
</body>
</html>