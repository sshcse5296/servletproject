<%@ include file="jstlTage.jsp"%>
<%@ page import="com.servlet.user.User"%>
<header>
<ul>
	<li><a class="active" href="/CourseManagement/index.jsp">Home</a></li>
	<li><a href="/CourseManagement/user/list">UserList</a></li>
	<li><a href="#contact">Contact</a></li>
	<li style="float: right">
	<%
	User loggedInUser=(User)request.getSession().getAttribute("user"); 
	if (loggedInUser== null) {%>
		<a href="/CourseManagement/login.jsp">Sign In</a></li>
	<%}else{%>
		<a href="/CourseManagement/logoutServlet">Logout</a>
		</li>
		<span class="user-detail"><%=loggedInUser.getUsername()%></span>
	<%}%>
	
</ul>
</header>