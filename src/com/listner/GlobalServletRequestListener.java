package com.listner;

import javax.servlet.ServletRequest;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@WebListener
public class GlobalServletRequestListener implements ServletRequestListener {

	public void requestInitialized(ServletRequestEvent servletRequestEvent) {
		System.out.println("ServletRequest initialized............. ");
	}

	public void requestDestroyed(ServletRequestEvent servletRequestEvent) {
		System.out.println("ServletRequest destroyed................ ");
	}

}
