package com.listner;

import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class GlobalServletContextAttributeListener implements ServletContextAttributeListener {

	public void attributeAdded(ServletContextAttributeEvent servletContextAttributeEvent) {
		System.out.println("ServletContext attribute added:");
	}

	public void attributeReplaced(ServletContextAttributeEvent servletContextAttributeEvent) {
		System.out.println("ServletContext attribute replaced:");
	}

	public void attributeRemoved(ServletContextAttributeEvent servletContextAttributeEvent) {
		System.out.println("ServletContext attribute removed:");
	}

}
