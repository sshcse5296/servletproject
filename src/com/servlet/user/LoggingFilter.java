package com.servlet.user;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
@WebFilter("/LoggingFilter")
public class LoggingFilter implements Filter {
	public void init(FilterConfig fConfig) throws ServletException {
			
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		PrintWriter out=response.getWriter();
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		if(username.isEmpty() && username.equals("")){
		out.println("Username can't be empty");	
		}
		else if(password.isEmpty() && password.equals("")){
		out.println("password can't be empty");	
		}
		else{
		chain.doFilter(request, response);
	    }
	
}
}
